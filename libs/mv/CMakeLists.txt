# Create a library called "Mv" which includes the source file "mv.c".
# The extension is already found. Any number of sources could be listed here.
add_library (Mv src/mv.c src/mv.h)

# Make sure the compiler can find include files for our Hello library
# when other libraries or executables link to Hello
target_include_directories (Mv PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})